# Hungarian translation for realmd.
# Copyright (C) 2014, 2016, 2019 Free Software Foundation, Inc.
# This file is distributed under the same license as the realmd package.
#
# Balázs Úr <ur.balazs at fsf dot hu>, 2014, 2019.
# Gabor Kelemen <kelemeng at ubuntu dot com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: realmd\n"
"Report-Msgid-Bugs-To: https://bugs.freedesktop.org/enter_bug.cgi?product=realm"
"d&keywords=I18N+L10N&component=General\n"
"POT-Creation-Date: 2019-01-23 20:04+0000\n"
"PO-Revision-Date: 2019-02-13 20:09+0100\n"
"Last-Translator: Balázs Úr <ur.balazs at fsf dot hu>\n"
"Language-Team: Hungarian <gnome-hu-list at gnome dot org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../service/org.freedesktop.realmd.policy.in.h:1
msgid "Discover realm"
msgstr "A tartomány feltérképezése"

#: ../service/org.freedesktop.realmd.policy.in.h:2
msgid "Authentication is required to discover a kerberos realm"
msgstr "Hitelesítés szükséges a kerberos tartomány feltérképezéséhez"

#: ../service/org.freedesktop.realmd.policy.in.h:3
msgid "Join machine to realm"
msgstr "Gép csatlakoztatása a tartományhoz"

#: ../service/org.freedesktop.realmd.policy.in.h:4
msgid "Authentication is required to join this machine to a realm or domain"
msgstr "Hitelesítés szükséges a gép csatlakoztatásához a tartományhoz"

#: ../service/org.freedesktop.realmd.policy.in.h:5
msgid "Remove machine from realm"
msgstr "Gép eltávolítása a tartományból"

#: ../service/org.freedesktop.realmd.policy.in.h:6
msgid ""
"Authentication is required to remove this computer from a realm or domain."
msgstr "Hitelesítés szükséges a számítógép eltávolításához a tartományból."

#: ../service/org.freedesktop.realmd.policy.in.h:7
msgid "Change login policy"
msgstr "Bejelentkezési házirend módosítása"

#: ../service/org.freedesktop.realmd.policy.in.h:8
msgid ""
"Authentication is required to change the policy of who can log in on this "
"computer."
msgstr ""
"Hitelesítés szükséges a házirend módosításához, hogy ki jelentkezhet be erre "
"a számítógépre."

#: ../service/realm-command.c:347
#, c-format
msgid "Process was terminated with signal: %d"
msgstr "A folyamat a következő szignállal fejeződött be: %d"

#: ../service/realm-command.c:400 ../service/realm-ldap.c:350
#, c-format
msgid "The operation was cancelled"
msgstr "A művelet megszakítva"

#: ../service/realm-command.c:577
#, c-format
msgid "Configured command not found: %s"
msgstr "Nem található beállított parancs: %s"

#: ../service/realm-command.c:582
#, c-format
msgid "Skipped command: %s"
msgstr "Kihagyott parancs: %s"

#: ../service/realm-command.c:588
#, c-format
msgid "Configured command invalid: %s"
msgstr "A beállított parancs érvénytelen: %s"

#: ../service/realm-disco-mscldap.c:198
#, c-format
msgid "Received invalid or unsupported Netlogon data from server"
msgstr "Érvénytelen vagy nem támogatott Netlogon adat érkezett a kiszolgálótól"

#: ../service/realm-disco-mscldap.c:346
msgid "LDAP on this system does not support UDP connections"
msgstr "Az ezen a rendszeren lévő LDAP nem támogatja az UDP kapcsolatokat"

#: ../service/realm-example.c:82 ../service/realm-samba.c:252
#: ../service/realm-sssd-ad.c:316 ../service/realm-sssd-ipa.c:300
#, c-format
msgid "Unsupported or unknown membership software '%s'"
msgstr "Nem támogatott vagy ismeretlen tagsági szoftver: „%s”"

#: ../service/realm-example.c:170 ../service/realm-samba.c:287
msgid "Already joined to a domain"
msgstr "Már csatlakozott egy tartományhoz"

#: ../service/realm-example.c:178 ../service/realm-example.c:271
msgid "Admin name or password is not valid"
msgstr "Az adminisztrátor neve vagy jelszava nem érvényes"

#: ../service/realm-example.c:242 ../service/realm-samba.c:427
#: ../service/realm-sssd-ad.c:521
msgid "Not currently joined to this domain"
msgstr "Jelenleg nincs csatlakozva ehhez a tartományhoz"

#: ../service/realm-example.c:301
msgid "Need credentials for leaving this domain"
msgstr "Hitelesítési adatok szükségesek a tartomány elhagyásához"

#: ../service/realm-ini-config.c:736
#, c-format
msgid "Couldn't write out config: %s"
msgstr "Nem sikerült a beállítások kiírása: %s"

#: ../service/realm-invocation.c:536
msgid "Not authorized to perform this action"
msgstr "Nincs felhatalmazva a művelet végrehajtására"

#: ../service/realm-kerberos.c:130 ../service/realm-kerberos.c:208
#: ../service/realm-provider.c:158
msgid "Operation was cancelled."
msgstr "A művelet megszakítva."

#: ../service/realm-kerberos.c:135
msgid "Failed to enroll machine in realm. See diagnostics."
msgstr ""
"Nem sikerült a gép bejegyzése a tartományba. Nézze meg a diagnosztikát."

#: ../service/realm-kerberos.c:213
msgid "Failed to unenroll machine from domain. See diagnostics."
msgstr ""
"Nem sikerült a gép bejegyzésének törlése a tartományból. Nézze meg a "
"diagnosztikát."

#: ../service/realm-kerberos.c:273
msgid "Joining this realm without credentials is not supported"
msgstr "A tartományba csatlakozás hitelesítési adatok nélkül nem támogatott"

#: ../service/realm-kerberos.c:274
msgid "Leaving this realm without credentials is not supported"
msgstr "A tartomány elhagyása hitelesítési adatok nélkül nem támogatott"

#: ../service/realm-kerberos.c:277
msgid "Joining this realm using a credential cache is not supported"
msgstr ""
"A tartományba csatlakozás hitelesítési adatok gyorsítótárának használatával "
"nem támogatott"

#: ../service/realm-kerberos.c:278
msgid "Leaving this realm using a credential cache is not supported"
msgstr ""
"A tartomány elhagyása hitelesítési adatok gyorsítótárának használatával nem "
"támogatott"

#: ../service/realm-kerberos.c:281
msgid "Joining this realm using a secret is not supported"
msgstr "A tartományba csatlakozás titok használatával nem támogatott"

#: ../service/realm-kerberos.c:282
msgid "Unenrolling this realm using a secret is not supported"
msgstr "A tartomány bejegyzésének törlése titok használatával nem támogatott"

#: ../service/realm-kerberos.c:285
msgid "Enrolling this realm using a password is not supported"
msgstr "A tartományba bejegyzés jelszó használatával nem támogatott"

#: ../service/realm-kerberos.c:286
msgid "Unenrolling this realm using a password is not supported"
msgstr "A tartomány bejegyzésének törlése jelszó használatával nem támogatott"

#: ../service/realm-kerberos.c:312
msgid "Joining this realm is not supported"
msgstr "A tartományba csatlakozás nem támogatott"

#: ../service/realm-kerberos.c:313
msgid "Leaving this realm is not supported"
msgstr "A tartomány elhagyása nem támogatott"

#: ../service/realm-kerberos.c:333 ../service/realm-kerberos.c:505
msgid "Already running another action"
msgstr "Már fut egy másik művelet"

#: ../service/realm-kerberos.c:376
#, c-format
msgid "Already joined to another domain: %s"
msgstr "Már csatlakozott egy másik tartományhoz: %s"

#: ../service/realm-kerberos.c:447
msgid "Failed to change permitted logins. See diagnostics."
msgstr ""
"Nem sikerült az engedélyezett bejelentkezések módosítása. Nézze meg a "
"diagnosztikát."

#: ../service/realm-kerberos.c:748
#, c-format
msgid "The realm does not allow specifying logins"
msgstr "A tartomány nem engedélyezi a bejelentkezések megadását"

#: ../service/realm-kerberos.c:756
#, c-format
msgid "Invalid login argument%s%s%s does not match the login format."
msgstr ""
"Érvénytelen bejelentkezési argumentum: %s%s%s nem felel meg a bejelentkezési "
"formátumnak."

#: ../service/realm-packages.c:389
#, c-format
msgid "The following packages are not available for installation: %s"
msgstr "A következő csomagok nem telepíthetők: %s"

#: ../service/realm-packages.c:471 ../service/realm-packages.c:506
#, c-format
msgid "Necessary packages are not installed: %s"
msgstr "A szükséges csomagok nincsenek telepítve: %s"

#.
#. * Various people have been worried by installing packages
#. * quietly, so notify about what's going on.
#. *
#. * In reality *configuring* and *starting* a daemon is far
#. * more worrisome than the installation. It's realmd's job
#. * to configure, enable and start stuff. So if you're properly
#. * worried, remove realmd and do stuff manually.
#.
#: ../service/realm-packages.c:477 ../tools/realm-client.c:143
msgid "Installing necessary packages"
msgstr "Szükséges csomagok telepítése"

#: ../service/realm-provider.c:163
msgid "Failed to discover realm. See diagnostics."
msgstr "Nem sikerült a tartomány felderítése. Nézze meg a diagnosztikát."

#: ../service/realm-samba.c:472
#, c-format
msgid "Not joined to this domain"
msgstr "Nem csatlakozott ehhez a tartományhoz"

#: ../service/realm-samba.c:482 ../service/realm-samba.c:524
#, c-format
msgid "The Samba provider cannot restrict permitted logins."
msgstr ""
"A Samba szolgáltató nem korlátozhatja az engedélyezett bejelentkezéseket."

#: ../service/realm-sssd.c:130
#, c-format
msgid "Invalid login argument '%s' contains unsupported characters."
msgstr ""
"Érvénytelen bejelentkezési argumentum: „%s”. Nem támogatott karaktereket "
"tartalmaz."

#: ../service/realm-sssd-ad.c:126
#, c-format
msgid "Enabling SSSD in nsswitch.conf and PAM failed."
msgstr "Az SSSD engedélyezése az nsswitch.conf-ban és a PAM-ban meghiúsult."

#: ../service/realm-sssd-ad.c:247
#, c-format
msgid "Unable to automatically join the domain"
msgstr "Nem lehet automatikusan csatlakozni a tartományba"

#: ../service/realm-sssd-ad.c:333
#, c-format
msgid ""
"Joining a domain with a one time password is only supported with the '%s' "
"membership software"
msgstr ""
"Tartományhoz csatlakozás egyszeri jelszóval csak a(z) „%s” tagsági "
"szoftverrel támogatott"

#: ../service/realm-sssd-ad.c:347
#, c-format
msgid ""
"Joining a domain with a user password is only supported with the '%s' "
"membership software"
msgstr ""
"Tartományhoz csatlakozás felhasználói jelszóval csak a(z) „%s” tagsági "
"szoftverrel támogatott"

#: ../service/realm-sssd-ad.c:363
#, c-format
msgid "Unsupported credentials for joining a domain"
msgstr "Nem támogatott hitelesítési adatok a tartományhoz csatlakozáshoz"

#: ../service/realm-sssd-ad.c:405 ../service/realm-sssd-ipa.c:304
#: ../tools/realm-join.c:240
msgid "Already joined to this domain"
msgstr "Már csatlakozott ehhez a tartományhoz"

#: ../service/realm-sssd-ad.c:409 ../service/realm-sssd-ipa.c:308
msgid "A domain with this name is already configured"
msgstr "Már be van állítva egy tartomány ezzel a névvel"

#: ../service/realm-sssd-config.c:149
#, c-format
msgid "Already have domain %s in sssd.conf config file"
msgstr "Az sssd.conf beállítófájl már tartalmazza ezt a tartományt: %s"

#: ../service/realm-sssd-config.c:198
#, c-format
msgid "Don't have domain %s in sssd.conf config file"
msgstr "Az sssd.conf beállítófájl nem tartalmazza ezt a tartományt: %s"

#: ../service/realm-sssd-ipa.c:294
msgid "The computer-ou argument is not supported when joining an IPA domain."
msgstr ""
"A computer-ou argumentum nem támogatott IPA tartományhoz csatlakozáshoz."

#: ../service/realm-sssd-ipa.c:462
msgid "Not currently joined to this realm"
msgstr "Jelenleg nincs csatlakozva ehhez a tartományhoz"

#: ../tools/realm.c:40
msgid "Discover available realm"
msgstr "Elérhető tartomány feltérképezése"

#: ../tools/realm.c:41
msgid "Enroll this machine in a realm"
msgstr "Gép bejegyzése egy tartományba"

#: ../tools/realm.c:42
msgid "Unenroll this machine from a realm"
msgstr "Gép bejegyzésének törlése egy tartományból"

#: ../tools/realm.c:43
msgid "List known realms"
msgstr "Ismert tartományok kiírása"

#: ../tools/realm.c:44
msgid "Permit user logins"
msgstr "Felhasználói bejelentkezések engedélyezése"

#: ../tools/realm.c:45
msgid "Deny user logins"
msgstr "Felhasználói bejelentkezések tiltása"

#: ../tools/realm.c:182
#, c-format
msgid "Invalid value for %s option: %s"
msgstr "Érvénytelen érték a(z) %s kapcsolóhoz: %s"

#: ../tools/realm.c:214
msgid "Install mode to a specific prefix"
msgstr "Telepítési mód egy konkrét előtaghoz"

#: ../tools/realm.c:215
msgid "Verbose output"
msgstr "Részletes kimenet"

#: ../tools/realm.c:216
msgid "Do not prompt for input"
msgstr "Ne kérjen adatbevitelt"

#: ../tools/realm-client.c:193 ../tools/realm-client.c:203
msgid "Couldn't connect to realm service"
msgstr "Nem lehet csatlakozni a tartományszolgáltatáshoz"

#: ../tools/realm-client.c:232
msgid "Couldn't load the realm service"
msgstr "Nem lehet betölteni a tartományszolgáltatást"

#: ../tools/realm-client.c:251
msgid "Couldn't connect to system bus"
msgstr "Nem sikerült a rendszerbuszhoz kapcsolódni"

#: ../tools/realm-client.c:281
#, c-format
msgid "Couldn't create socket pair: %s"
msgstr "A socketpár létrehozása sikertelen: %s"

#: ../tools/realm-client.c:289 ../tools/realm-client.c:321
msgid "Couldn't create socket"
msgstr "A socket létrehozása sikertelen"

#: ../tools/realm-client.c:302
msgid "Couldn't run realmd"
msgstr "A realmd nem futtatható"

#: ../tools/realm-client.c:567
#, c-format
msgid "Couldn't create runtime directory: %s: %s"
msgstr "Nem hozható létre a futtatókörnyezet „%s” könyvtára: %s"

#: ../tools/realm-client.c:577
#, c-format
msgid "Couldn't create credential cache file: %s: %s"
msgstr "Nem hozható létre a hitelesítési adatok gyorsítótárfájlja: %s: %s"

#: ../tools/realm-client.c:587
msgid "Couldn't resolve credential cache"
msgstr "Nem oldható fel a hitelesítési adatok gyorsítótára"

#: ../tools/realm-client.c:677
#, c-format
msgid "Invalid password for %s"
msgstr "A jelszó érvénytelen ehhez: %s"

#: ../tools/realm-client.c:681
#, c-format
msgid "Couldn't authenticate as %s"
msgstr "Nem sikerült a hitelesítés mint %s"

#: ../tools/realm-client.c:706
#, c-format
msgid "Couldn't parse user name: %s"
msgstr "A felhasználónév nem dolgozható fel: %s"

#: ../tools/realm-client.c:730
msgid "Couldn't read credential cache"
msgstr "Nem olvasható a hitelesítési adatok gyorsítótára"

#: ../tools/realm-client.c:756 ../tools/realm-client.c:963
msgid "Couldn't initialize kerberos"
msgstr "A kerberos nem készíthető elő"

#: ../tools/realm-client.c:819
#, c-format
msgid "Cannot prompt for a password when running in unattended mode"
msgstr "Nem lehet jelszót kérni felügyelet nélküli módban futáskor"

#: ../tools/realm-client.c:823
#, c-format
msgid "Password for %s: "
msgstr "%s jelszava: "

#: ../tools/realm-client.c:841
#, c-format
msgid "Couldn't prompt for password: %s"
msgstr "Nem lehet bekérni a jelszót: %s"

#: ../tools/realm-client.c:876
#, c-format
msgid "Realm does not support membership using a password"
msgstr "A tartomány nem támogatja a tagságot jelszó használatával"

#: ../tools/realm-client.c:915
#, c-format
msgid "Realm does not support membership using a one time password"
msgstr "A tartomány nem támogatja a tagságot egyszeri jelszó használatával"

#: ../tools/realm-client.c:983
msgid "Couldn't select kerberos credentials"
msgstr "Nem sikerült a kerberos hitelesítési adatok kiválasztása"

#: ../tools/realm-client.c:997
msgid "Couldn't read kerberos credentials"
msgstr "Nem sikerült a kerberos hitelesítési adatok olvasása"

#: ../tools/realm-client.c:1047
#, c-format
msgid "Realm does not support automatic membership"
msgstr "A tartomány nem támogatja az automatikus tagságot"

#: ../tools/realm-discover.c:135
msgid "Couldn't discover realms"
msgstr "Nem sikerült a tartományok felderítése"

#: ../tools/realm-discover.c:155
msgid "No default realm discovered"
msgstr "Nem lett felderítve alapértelmezett tartomány"

#: ../tools/realm-discover.c:157
#, c-format
msgid "No such realm found: %s"
msgstr "Nem található ilyen tartomány: %s"

#: ../tools/realm-discover.c:181
msgid "Show all discovered realms"
msgstr "Minden felderített tartomány megjelenítése"

#: ../tools/realm-discover.c:182 ../tools/realm-discover.c:273
msgid "Show only the names"
msgstr "Csak a nevek megjelenítése"

#: ../tools/realm-discover.c:183 ../tools/realm-join.c:292
#: ../tools/realm-leave.c:266
msgid "Use specific client software"
msgstr "Konkrét kliensszoftver használata"

#: ../tools/realm-discover.c:184 ../tools/realm-join.c:298
msgid "Use specific membership software"
msgstr "Konkrét tagsági szoftver használata"

#: ../tools/realm-discover.c:185 ../tools/realm-join.c:308
#: ../tools/realm-leave.c:269
msgid "Use specific server software"
msgstr "Konkrét kiszolgálószoftver használata"

#: ../tools/realm-discover.c:272
msgid "Show all realms"
msgstr "Minden tartomány megjelenítése"

#: ../tools/realm-join.c:92 ../tools/realm-join.c:132 ../tools/realm-join.c:163
msgid "Couldn't join realm"
msgstr "Nem sikerült csatlakozni a tartományhoz"

#: ../tools/realm-join.c:231
msgid "Cannot join this realm"
msgstr "Nem lehet csatlakozni a tartományhoz"

#: ../tools/realm-join.c:233
msgid "No such realm found"
msgstr "Nem található ilyen tartomány"

#: ../tools/realm-join.c:290
msgid "Turn off automatic id mapping"
msgstr "Automatikus azonosítóleképezés kikapcsolása"

#: ../tools/realm-join.c:294
msgid "Use specific computer name instead of hostname"
msgstr "Konkrét számítógépnév használata a gépnév helyett"

#: ../tools/realm-join.c:296
msgid "Computer OU DN to join"
msgstr "Csatlakozás az ezzel az OU DN-nel rendelkező számítógéphez"

#: ../tools/realm-join.c:300
msgid "Join automatically without a password"
msgstr "Csatlakozás automatikusan jelszó nélkül"

#: ../tools/realm-join.c:302
msgid "Join using a preset one time password"
msgstr "Csatlakozás előre beállított egyszeri jelszóval"

#: ../tools/realm-join.c:304
msgid "Use specific operation system name"
msgstr "Konkrét operációs rendszer név használata"

#: ../tools/realm-join.c:306
msgid "Use specific operation system version"
msgstr "Konkrét operációs rendszer verzió használata"

#: ../tools/realm-join.c:310
msgid "User name to use for enrollment"
msgstr "A bejegyzéshez használandó felhasználónév"

#: ../tools/realm-join.c:312
msgid "Set the user principal for the computer account"
msgstr "A felhasználó résztvevő beállítása a számítógépes fiókhoz"

#: ../tools/realm-join.c:332
msgid "Specify one realm to join"
msgstr "Adjon meg egy tartományt a csatlakozáshoz"

#: ../tools/realm-join.c:337
msgid ""
"The --no-password argument cannot be used with --one-time-password or --user"
msgstr ""
"A --no-password kapcsoló nem használható a --one-time-password vagy --user "
"kapcsolókkal"

#: ../tools/realm-join.c:342
msgid "The --one-time-password argument cannot be used with --user"
msgstr "A --one-time-password kapcsoló nem használható --user kapcsolóval"

#: ../tools/realm-leave.c:181 ../tools/realm-leave.c:208
msgid "Couldn't leave realm"
msgstr "Nem sikerült elhagyni a tartományt"

#: ../tools/realm-leave.c:267
msgid "Remove computer from realm"
msgstr "Számítógép eltávolítása a tartományból"

#: ../tools/realm-leave.c:270
msgid "User name to use for removal"
msgstr "Az eltávolításhoz használandó felhasználónév"

#: ../tools/realm-logins.c:129 ../tools/realm-logins.c:175
msgid "Couldn't change permitted logins"
msgstr "Nem sikerült az engedélyezett bejelentkezések módosítása"

#: ../tools/realm-logins.c:200
msgid "Permit any realm account login"
msgstr "Bármely tartományi fiók bejelentkezésének engedélyezése"

#: ../tools/realm-logins.c:200
msgid "Deny any realm account login"
msgstr "Bármely tartományi fiók bejelentkezésének tiltása"

#: ../tools/realm-logins.c:202
msgid "Withdraw permit for a realm account to login"
msgstr "Egy tartományi fiók bejelentkezési engedélyének visszavonása"

#: ../tools/realm-logins.c:204
msgid "Treat names as groups which to permit"
msgstr "Nevek kezelése engedélyezendő csoportokként"

#: ../tools/realm-logins.c:205
msgid "Realm to permit/deny logins for"
msgstr "Bejelentkezések engedélyezése/tiltása ezen tartományhoz"

#: ../tools/realm-logins.c:219
msgid "No logins should be specified with -a or --all"
msgstr "A -a vagy --all kapcsolóval nem adható meg bejelentkezési név"

#: ../tools/realm-logins.c:222
msgid "The --withdraw or -x arguments cannot be used when denying logins"
msgstr ""
"A --withdraw vagy -x kapcsolók nem használhatók bejelentkezések letiltásakor"

#: ../tools/realm-logins.c:225
msgid "Specific logins must be specified with --withdraw"
msgstr "A konkrét bejelentkezéseket a --withdraw kapcsolóval kell megadni"

#: ../tools/realm-logins.c:228
msgid "Groups may not be specified with -a or --all"
msgstr "A -a vagy --all kapcsolóval nem adható meg csoport"

#: ../tools/realm-logins.c:235
msgid "Use --all to deny all logins"
msgstr "Az összes bejelentkezés letiltásához használja a --all kapcsolót"

#: ../tools/realm-logins.c:237
msgid "Specify specific users to add or remove from the permitted list"
msgstr ""
"Adjon meg konkrét felhasználókat az engedélyezettek listájához adáshoz vagy "
"onnan eltávolításhoz"

#: ../tools/realm-logins.c:241
msgid ""
"Specifying deny without --all is deprecated. Use realm permit --withdraw"
msgstr ""
"A tiltás megadása elavult a --all használata nélkül. Használja a realm "
"permit --withdraw parancsot"
